<?php
namespace App\Http\Controllers;
use \Illuminate\Http\Request;

class MoviesController extends Controller
{
	public function viewList()
	{
		return view('list-movies');
	}
	public function showDetails()
	{
		// return view('')

	}
	public function addMovie()
	{
		return view('addMovie');
	}
	public function saveMovie(request $request)
	{
		$rules = [
		'name' => 'required|alpha',
		'year' => 'required|date_format:"Y"|before:"now"',
		'director' => 'required|alpha',
		'actor' => 'required|alpha'
		];
		$validator = \Validator::make($request->all(),$rules);
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator);
			}
	}
}