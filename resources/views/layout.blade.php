<!DOCTYPE html>
<html>
<head>
	<title> {{ $title or 'Naslov' }}</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
	
		@section ('content')
		@show

</body>
</html>