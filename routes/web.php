<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/list-movies')->name('list-movies')->uses('MoviesController@viewList');
// Route::get('/details/{id}')->name('details')->uses('MoviesController@showDetails');
Route::get('/addMovie')->name('addMovie')->uses('MoviesController@addMovie');
Route::post('/list-movies')->uses('MoviesController@saveMovie');